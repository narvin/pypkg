=====
pypkg
=====

A Python template package with:

- packaging files so the package can be installed in editable mode and
  imported easily
- linting using `pylint`
- PEP 8 code stye checking `pycodestyle`
- type checking using `mypy`
- unit testing using `pytest` with tests named:

  - `<module>_test.py` located next to `<module>.py`
  - `test_<module>.py` located in a parallel `tests` dir

- deployment using `build` and `twine`

All checks and tests can be run using `make`.

Installation
============

Installing the package in editable mode makes it so you can import the
package without having to worry about relative imports, or whether any module
importing the package is run as a file, or as a module.

Clone the Repository
--------------------

::

  git clone https://gitlab.com/narvin/pypkg.git
  cd pypkg

Setup the Virtual Environment
-----------------------------

::

  python -m venv .venv             # create the venv in the dir `.venv`
  source ./venv/bin/activate       # activate the venv on Linux/Unix/macOS
  .\venv\Scripts\activate          # activate the venv on Windows
  pip install -r requirements.txt  # install dev dependencies
  pip install -e .                 # install this package in editable mode

Usage
=====

The following examples all assume the current working dir is the project root.

Run Checks/Tests Started Manually
---------------------------------

**Note:** If you don't have a separate `tests` dir, you should omit it from
the following examples.

Lint all the source files:

::

  pylint src tests

PEP 8 style check all the source files:

::

  pycodestyle src tests

Type check all the source files:

::

  mypy src tests

Run all tests:

::

  pytest src tests

Run Checks/Tests with GNU Make
------------------------------

**Note:** These `make` examples probably won't work on Windows.

Run all checks/tests on all source files:

::

  make

This is equivalent to running these individual checks/tests on all source
files:

::

  make lint
  make style
  make type
  make test

Run all checks/tests on a single module if the checks/tests haven't succeeded
since the module was last modified:

::

  make <module_file>

Run all checks on a single test module, then run the test if the checks
haven't succeeded since the test was last modified:

::

  make <test_file>

Run all checks/tests on any modules if the checks/tests haven't succeeded
since that module was last modified:

::

  make modules

Run all checks on any test modules, then run that test if the checks haven't
succeeded since that test was last modified:

::

  make tests

Delete cache files:

::

  make clean

Print the variables defined in the Makefile:

::

  make debug

Deployment
==========

Build the package from the project root:

::

  python -m build

Or build the package using `make`:

::

  make build

Deploy the package to the test repo:

::

  twine upload --repository testpypi dist/*

Or deploy the the current version of the package using `make`:

::

  make deploy

To re-upload the package after you've made changes, you have to:

1. Increment the version in `setup.cfg`.
2. Remove previous build files from the `dist` dir (or remove the entire
   `dist` dir). This step is not necessary if using `make deploy`, which
   will only deploy files for the version in `setup.cfg`.
3. Rebuild the package.

Install the package from the test repo (replace `pypkg-narvin` with the
package name you've set in `setup.cfg`):

::

  pip install -i https://test.pypi.org/simple/ pypkg-narvin

