"""A package module"""


def mod_fn() -> None:
    """A module function"""
    print("hello from mod_fn")


if __name__ == "__main__":
    mod_fn()

