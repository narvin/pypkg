"""A package module"""


def my_fn() -> None:
    """A module function"""
    print("hello from my_fn")


if __name__ == "__main__":
    my_fn()

