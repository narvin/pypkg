""" A module test located next to the module """

import glob
import os
import shutil
import subprocess
import tempfile
from typing import Generator, Tuple
from _pytest.fixtures import SubRequest
from _pytest.python import Metafunc
import pytest


class TestImport:
    """Module can be imported from files and modules in various locations"""

    proj_dirs = ["./", "./src", "./src/mypkg"]
    dirs: list[str] = []
    tmp_dirs: list[str] = []
    is_dirs_ready = False

    @staticmethod
    def pytest_generate_tests(metafunc: Metafunc) -> None:
        """
        Create a single temp dir in the project dir, and a single temp dir
        outside the project dir for the all tests in the class, add those
        dirs to the list of dirs, and parametrize the temp_file fixture,
        and the cwd argument of the test_file_imports_module function with
        those dirs at collection time
        """
        if not TestImport.is_dirs_ready:
            TestImport.tmp_dirs.append(tempfile.mkdtemp(prefix="pytest_",
                                       dir=TestImport.proj_dirs[-1]))
            TestImport.tmp_dirs.append(tempfile.mkdtemp(prefix="pytest_"))
            TestImport.dirs = [*TestImport.proj_dirs, *TestImport.tmp_dirs]
            TestImport.is_dirs_ready = True
        if "temp_file" in metafunc.fixturenames:
            metafunc.parametrize("temp_file", TestImport.dirs, indirect=True)
        if "test_file_imports_module" == metafunc.function.__name__:
            metafunc.parametrize("cwd", TestImport.dirs)

    @staticmethod
    @pytest.fixture(scope="class", autouse=True)
    def remove_temp_dir() -> Generator[None, None, None]:
        """Remove temp and cache dirs after all tests have run"""
        yield
        for proj_dir in TestImport.proj_dirs:
            cache_dir = os.path.join(proj_dir, "__pycache__")
            if os.path.isdir(cache_dir) and not os.listdir(cache_dir):
                os.rmdir(cache_dir)
        for tmp_dir in TestImport.tmp_dirs:
            shutil.rmtree(tmp_dir)

    @staticmethod
    @pytest.fixture
    def temp_file(request: SubRequest) -> Generator[str, None, None]:
        """
        Create a temporary file that imports our module.

        pytest_generate_tests parameterized request at collection time,
        so this fixture will create a file in each dir in TestImport.dirs.
        """
        module_dir = request.param
        with tempfile.NamedTemporaryFile(suffix=".py", dir=module_dir,
                                         delete=False) as module_fp:
            if module_dir != "./src/mypkg":
                module_fp.write(b"from mypkg import mypkg\nmypkg.my_fn()\n")
            # If the importing module is in the same dir as the imported
            # module, the module will be imported from the dir, so we have
            # to alter the import statement
            else:
                module_fp.write(b"import mypkg\nmypkg.my_fn()\n")
        yield module_fp.name
        cache_dir = os.path.join(os.path.dirname(module_fp.name), "__pycache__")
        cache_basename, _ = os.path.splitext(os.path.basename(module_fp.name))
        cache_pattern = f"{os.path.join(cache_dir, cache_basename)}*"
        for cache_file in glob.glob(cache_pattern):
            os.remove(cache_file)
        os.remove(module_fp.name)

    @staticmethod
    @pytest.fixture
    def temp_module(temp_file: str) -> Tuple[str, str]:
        """
        Create a temporary file that imports our module.

        pytest_generate_tests parameterized the temp_file request argument
        at collection time to vary the dir where the importing module is
        created, so this fixture will return a dir where the file is located,
        and file name pair for each file that temp_file creates.
        """
        module_dir, module_name = os.path.split(temp_file)
        return (module_dir, module_name[:-3])

    @staticmethod
    def test_file_imports_module(cwd: str, temp_file: str) -> None:
        """
        Test another module run as a file can import our module.

        pytest_generate_tests parameterized cwd, and the temp_file request
        argument at collection time to vary the dir where Python is started,
        and where the importing module is created, so this test will run once
        for each starting dir and file combination that cwd and temp_file
        returns.
        """
        res = subprocess.run(["python", temp_file],
                             capture_output=True, cwd=cwd, check=True)
        assert res.stdout == b"hello from my_fn\n"

    @staticmethod
    def test_module_imports_module(temp_module: Tuple[str, str]) -> None:
        """
        Test another module run as a module can import our module.

        This test will start Python in the dir where the file is located, and
        run the file for each dir and file name pair that temp_module returns.
        """
        module_dir, module_name = temp_module
        res = subprocess.run(["python", "-m", module_name],
                             capture_output=True, cwd=module_dir, check=True)
        assert res.stdout == b"hello from my_fn\n"

